﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TryahovLabs.Startup))]
namespace TryahovLabs
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
