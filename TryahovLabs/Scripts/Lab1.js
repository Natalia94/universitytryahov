﻿var xmlDoc,
    outputFilename = "Output.xml",
    arrayButDelName = [];

$(document).ready(function () {
    setInterval('inspectedEnteredData()', 1000);
});

function createTable()
{
    document.getElementById("buttonReadFile").disabled = true;

    var buttonWriteInFile = document.createElement("input");
    buttonWriteInFile.setAttribute("type", "button");
    buttonWriteInFile.setAttribute("id", "butWriteInFile");
    buttonWriteInFile.setAttribute("value", "Write in file");
    buttonWriteInFile.setAttribute("onclick", "writeInXMLFile('Output.xml', 'text/plain')");


    var link = document.createElement("a");
    link.setAttribute("id", "linkClick");
    link.setAttribute("href", "");
    link.setAttribute("value", "click here to download your file");

    var textCell;
    var div = document.getElementsByTagName("div")[5];
    var table = document.createElement("table");
    table.id = "tableParametr";

    for (var i = 0; i < countRow() + 1; i++) 
    {
        var row = document.createElement("tr");

        for (var j = 0; j < 7; j++) 
        {
            var column = document.createElement("td");

            if (i == 0 || j == 0) {
                textCell = document.createTextNode(addTextToTableMenu(i, j));
                column.appendChild(textCell);
            }
            else
            {
                if (j == 4) column.appendChild(addComboBoxToType(i));
                else if (j == 5)
                {
                    var divToElement = document.createElement("div");
                    divToElement.setAttribute("id", "divToElement" + i);
                    divToElement.appendChild(addTextBoxToValue(i));
                    column.appendChild(divToElement);
                }
                else if (j == 6) column.appendChild(addButtonToDelete(i));
                else
                {
                    textCell = document.createTextNode(addTextInTable(i, j));
                    column.appendChild(textCell);
                }
            }
            row.appendChild(column);
        }
        table.appendChild(row);
    }
    div.appendChild(buttonWriteInFile);
    div.appendChild(table);
    table.setAttribute("border", 2);
};

function countRow() 
{
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", "/Xml/Input.xml", false);
    xmlhttp.send("");
    xmlDoc = xmlhttp.responseXML;
    var parametr = xmlDoc.getElementsByTagName("Parameter");
    return parametr.length;
};

function addTextToTableMenu(i, j)
{
    if (i == 0) {
        if (j == 0) return "Parametr";
        if (j == 1) return "ID";
        if (j == 2) return "Name";
        if (j == 3) return "Description";
        if (j == 4) return "Type";
        if (j == 5) return "Value";
        if (j == 6) return "Delete parametr";
    }
    if (j == 0 && i != 0) {
        return "Parametr" + i;
    }
};

function addTextInTable(i, j) 
{
    if (j - 1 == 0) return xmlDoc.getElementsByTagName("Id")[i - 1].childNodes[0].nodeValue;
    if (j - 1 == 1) return xmlDoc.getElementsByTagName("Name")[i - 1].childNodes[0].nodeValue;
    if (j - 1 == 2) return xmlDoc.getElementsByTagName("Description")[i - 1].childNodes[0].nodeValue;
};

function addComboBoxToType(index)
{
    var combobox = document.createElement("select");
    var arrayTypes = addVariablTypesInComboBox(index);

    for (var k = 0; k < arrayTypes.length; k++)
    {
        var option = document.createElement("option");
        option.text = arrayTypes[k];
        combobox.add(option, k);
    }
    combobox.setAttribute("id", "comboboxType" + index);

    if (arrayTypes[0] == "System.Boolean") {
        combobox.setAttribute("onclick", "clickComboBox(" + index + ");");
    } else {
        combobox.setAttribute("onclick", "clickComboBox(" + index + ");");
    }
    return combobox;
};

function addButtonToDelete(i)
{
    var buttonDelParametr = document.createElement("input");
    buttonDelParametr.setAttribute("type", "button");
    buttonDelParametr.setAttribute("id", "butDel" + i);
    buttonDelParametr.setAttribute("onclick", "deleteRow(" + i + ");");
    buttonDelParametr.setAttribute("value", "Delete parametr " + i);
    arrayButDelName[i-1] = "butDel" + i;
    return buttonDelParametr;
};

function addVariablTypesInComboBox(index)
{
    var arrayTypes = [];
    var theMainElement;
    
    for (var i = 0; i < countRow() ; i++) 
    {
        var type = xmlDoc.getElementsByTagName("Type")[i].childNodes[0].nodeValue;
        var tmp = 0;

        for (var j = 0; j < arrayTypes.length; j++)
        {
            if (arrayTypes[j] != type) tmp++;
        }

        if (tmp == arrayTypes.length)
        {
            arrayTypes[arrayTypes.length] = type;
        }

        if (i == index - 1) 
        {
            for (var j = 0; j < arrayTypes.length; j++) 
            {
                if (arrayTypes[j] == type) theMainElement = j;
            }
        }
    }
    return swapElements(0, theMainElement, arrayTypes);
};

function swapElements(indexLeft, indexRight, array) {
    var tempElement = array[indexLeft];
    array[indexLeft] = array[indexRight];
    array[indexRight] = tempElement;
    return array;
};

function addTextBoxToValue(i)
{
    
    var type = xmlDoc.getElementsByTagName("Type")[i - 1].childNodes[0].nodeValue;
    var value = xmlDoc.getElementsByTagName("Value")[i - 1].childNodes[0].nodeValue;
    
    if (type == "System.Boolean")
    {
        var divWithRadioButton = document.createElement("div");
        divWithRadioButton.setAttribute("id", "radioButton" + i);

        var textTrue = document.createTextNode(" True  ");
        var textFalse = document.createTextNode(" False");

        var radioButtonTrue = document.createElement("input");
        radioButtonTrue.setAttribute("type", "radio");
        radioButtonTrue.setAttribute("name", "radioButTrue" + i);

        var radioButtonFalse = document.createElement("input");
        radioButtonFalse.setAttribute("type", "radio");
        radioButtonFalse.setAttribute("name", "radioButFalse" + i);        

        if (value == "True") radioButtonTrue.checked = true;
        else radioButtonFalse.checked = true;
        
        divWithRadioButton.appendChild(radioButtonTrue);
        divWithRadioButton.appendChild(textTrue);
        divWithRadioButton.appendChild(radioButtonFalse);
        divWithRadioButton.appendChild(textFalse);

        return divWithRadioButton;
    }
    else
    {
        var textBox = document.createElement("input");
        
        textBox.setAttribute("value", value);
        textBox.setAttribute("id", "textBox" + i);
        if (type === "System.Int32") {
            textBox.setAttribute("type", "number");
            textBox.setAttribute("class", "checkEnteredData");
        }
        else {
            textBox.setAttribute("type", "text");
        }
        return textBox;
    }
};

function deleteRow(index) {
    document.getElementById("tableParametr").deleteRow(index);
    arrayButDelName.splice(index - 1, 1);
    updateIndexOnclick();
};

function updateIndexOnclick() {
    for (var i = 0; i < arrayButDelName.length; i++) {
        var buttonDelete = document.getElementById(arrayButDelName[i]);
        buttonDelete.setAttribute("onclick", "deleteRow(" + (i+1) + ");");
    }
};

function clickComboBox(index)
{
    var comboboxValue = document.getElementById("comboboxType" + index).value;

    var divElement = document.getElementById("divToElement" + index);
    divElement.removeChild(divElement.firstChild);

    if (comboboxValue === "System.String")
    {
        var divToElementString = document.getElementById("divToElement" + index);
        divToElementString.appendChild(addControlInTableToValue(index, "System.String"));
    }
    else if (comboboxValue === "System.Int32")
    {
        var divToElementInt = document.getElementById("divToElement" + index);
        divToElementInt.appendChild(addControlInTableToValue(index, "System.Int32"));
    }
    else
    {
        var divToElementBool = document.getElementById("divToElement" + index);
        divToElementBool.appendChild(addControlInTableToValue(index, "System.Boolean"));
    }
};

function addControlInTableToValue(i, type) {
    if (type === "System.Boolean")
    {
        var divWithRadioButton = document.createElement("div");
        divWithRadioButton.setAttribute("id", "radioButton" + i);

        var textTrue = document.createTextNode(" True  ");
        var textFalse = document.createTextNode(" False");

        var radioButtonTrue = document.createElement("input");
        radioButtonTrue.setAttribute("type", "radio");
        radioButtonTrue.setAttribute("name", "radioButTrue" + i);

        var radioButtonFalse = document.createElement("input");
        radioButtonFalse.setAttribute("type", "radio");
        radioButtonFalse.setAttribute("name", "radioButTrue" + i);

        radioButtonTrue.checked = true;

        divWithRadioButton.appendChild(radioButtonTrue);
        divWithRadioButton.appendChild(textTrue);
        divWithRadioButton.appendChild(radioButtonFalse);
        divWithRadioButton.appendChild(textFalse);

        return divWithRadioButton;
    }
    else {
        var textBox = document.createElement("input");
        
        textBox.setAttribute("id", "textBox" + i);
        if (type === "System.Int32") {
            textBox.setAttribute("type", "text");
            textBox.setAttribute("class", "checkEnteredData");
        }
        else {
            textBox.setAttribute("type", "text");
        }
        return textBox;
    }
};

function inspectedEnteredData() {
    $(".checkEnteredData").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
};

function writeInXMLFile(nameOutputFile, typeFile) {
    var div = addLinkDownload();
    var textToFile = textInOutputFile();
    if (textToFile == "Error!") {
        alert("Косяк в данных!");
    } else {
        var link = document.getElementById("linkClick");
        var file = new Blob([textToFile], { type: typeFile });

        link.href = URL.createObjectURL(file);
        link.download = nameOutputFile;

        document.getElementById('linkClick').click();
    }
};

function addLinkDownload() {
    var div = document.getElementsByTagName("div")[5];
    var link = document.createElement("a");

    link.setAttribute("id","linkClick");
    link.setAttribute("href","");
    div.appendChild(link);
    return div;
};

function textInOutputFile() {
    var text = "<?xml version='1.0' encoding='utf-8' ?> \n" +
        "<Parameters xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'> " + '\n';
    var table = document.getElementById("tableParametr");
    var countRows = table.getElementsByTagName("tr");
    var countColumns = countRows[1].getElementsByTagName("td");
    var number, comboboxValue, textValue;

    for (var i = 1; i < countRows.length; i++) {
        var columns = countRows[i].getElementsByTagName("td");
        text += "   <Parameter> \n";
        for (var j = 1; j < countColumns.length + 1; j++) {
            
            if (j == 1) text += "       <Id>" + columns[j].childNodes[0].nodeValue + "</Id> \n";
            if (j == 2) text += "       <Name>" + columns[j].childNodes[0].nodeValue + "</Name> \n";
            if (j == 3) text += "       <Description>" + columns[j].childNodes[0].nodeValue + "</Description> \n";
            if (j == 4) {
                number = columns[0].childNodes[0].nodeValue.substring(8);
                comboboxValue = document.getElementById("comboboxType" + number).value;

                text += "       <Type>" + comboboxValue + "</Type> \n";
            }
            if (j == 5) {
                number = columns[0].childNodes[0].nodeValue.substring(8);
                var element = document.getElementById("textBox" + number);
                if (comboboxValue == "System.String") {
                    if (element.value.length > 10) {
                        element.style.color = "#ff0000";
                        return "Error!";
                    } else {
                        element.style.color = "#000000";
                        textValue = element.value;
                    }
                }
                else if (comboboxValue == "System.Int32") {
                    if ((parseInt(element.value,10) > 1000) || (parseInt(element.value,10) < -1000)) {
                        element.style.color = "#ff0000";
                        return "Error!";
                    } else {
                        element.style.color = "#000000";
                        textValue = element.value;
                    }
                }
                else {
                    var divToElement = columns[j].childNodes[0].childNodes[0];
                    var radioButTrue = divToElement.childNodes[0];
                    if (radioButTrue.checked) textValue = "True";
                    else textValue = "False";
                }
                text += "       <Value>" + textValue + "</Value> \n";
            }
        }
        text += "   </Parameter> \n";
    }
    return text;
};

