﻿var arrayTest = [];

function addNewItem(newItem) {
    arrayTest.push(newItem);
}

function removeByIndex(index) {
    arrayTest.splice(index, 1);

    //delete arrayTest[index];
}

function removeByValue(value) {
    for (var i = 0; i < arrayTest.length; i++) {
        if (arrayTest[i] === value) {
            removeByIndex(i);
        }
    }
}

function addWithoutDuplication(newItem) {
    if (arrayTest.indexOf(newItem) === -1) {
        addNewItem(newItem);
    }
}

function reverseArray() {
    var tempElement;
    for (var i = 0; i < arrayTest.length / 2; i++) {
        swapElements(arrayTest.length - i - 1, i, arrayTest);
        //tempElement = arrayTest[arrayTest.length - i - 1];
        //arrayTest[arrayTest.length - i - 1] = arrayTest[i];
        //arrayTest[i] = tempElement;
    }
}

function sort(array) {
    for (var i = 0; i < array.length - 1; i++) {
        for (var j = 0; j < array.length - i - 1; j++) {
            if (array[j] > array[j + 1]) {
                swapElements(j, j + 1, array);
            }
        }
    }
}

function sortArray() {
    var stringArray = [];
    var boolArray = [];
    var numberArray = [];
    for (var i = 0; i < arrayTest.length; i++) {
        if (typeof arrayTest[i] === "boolean") {
            boolArray.push(arrayTest[i]);
        } else if (typeof arrayTest[i] === "number") {
            numberArray.push(arrayTest[i]);
        } else {
            stringArray.push(arrayTest[i]);
        }
    }

    sort(stringArray);
    sort(boolArray);
    sort(numberArray);
    arrayTest.splice(0, arrayTest.length);
    arrayTest = boolArray.concat(numberArray, stringArray);
}

function swapElements(indexLeft, indexRight, array) {
    var tempElement = array[indexLeft];
    array[indexLeft] = array[indexRight];
    array[indexRight] = tempElement;
}
